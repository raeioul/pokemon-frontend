import React, { useEffect, useState } from "react";
import axios from "axios";

export default function Pokemon() {
  const [pokemons, setPokemons] = useState([]);
  const [error, setError] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const pokemon = event.target.elements.pokemon.value;
    const exists = pokemons.some((p) => p.name === pokemon);
    if (!exists) {
      try {
        const result = await axios.get(
          `https://pokeapi.co/api/v2/pokemon/${pokemon}`
        );
        setPokemons([...pokemons, result.data]);
        setError("");
      } catch (error) {
        if (error.response.status === 404) {
          setError(`No tenemos ${pokemon}`);
        }
      }
    } else {
      setError(`${pokemon} is already in the list.`);
    }
  };

  return (
    <div className="App">
      <form onSubmit={handleSubmit}>
        <input type="text" name="pokemon"></input>
        <button>Send Pokemon!</button>
      </form>
      {pokemons
        ? pokemons.map((pokemon) => {
            return (
              <div>
                <img src={pokemon.sprites["front_default"]} />
                <img src={pokemon.sprites["back_default"]} />
                <img src={pokemon.sprites["front_shiny"]} />
                <p>{pokemon.name}</p>
              </div>
            );
          })
        : ""}
      {error && <div>{error}</div>}
    </div>
  );
}
